<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title><?php bloginfo('name');
        wp_title('|'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php wp_head(); ?>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/script.js"></script>
</head>
<body <?php body_class(); ?>>

<div class="wrapper container-fluid">
    <header
        style="background-image: url('<?php bloginfo('template_directory'); ?>/images/header/<?php echo rand(1, 4) ?>.jpg');">

        <div class="row logo">
            <div class="col-md-8 col-md-offset-2 logo">
                <img src="<?php bloginfo('template_directory'); ?>/images/logo.png" width="300px"/>
            </div>
            <!-- col-md-8 col-md-offset-2-->
        </div>
        <!-- ./row logo -->

        <nav class="menu" id="mainmenu">
            <div class="row">
                <div class="col-md-1 col-md-push-1 toggle">
                    <a onclick="toggleMenu()"><img
                            src="<?php bloginfo('template_directory'); ?>/images/toggle.png" width="30px"/></a>
                </div>
                <div class="col-md-1 col-md-push-1 boebel">
                    <img src="<?php bloginfo('template_directory'); ?>/images/boebel_weiss_1.png" width="35px"/>
                </div>
                <div class="col-md-8 col-md-push-1 menucontent">
                    <?php wp_nav_menu(array('theme_location' => 'main-menu', 'items_wrap' => '
                    <ul>%3$s</ul>', 'container' => 'none', 'link_after' => '<li class="separator">&middot;</li>')); ?>
                </div>
            </div>
        </nav>
        <!-- Show category description as slogan if the user is in a category archive -->

        <div class="row">
            <div class="col-md-8 col-md-offset-2 slogan">
                <?php
                if (is_category()) {
                    echo mb_strtoupper(category_description($category_id), 'UTF-8');
                } else {
                    echo mb_strtoupper('Unter den Namen besser:: veranstaltet das Münchner Schülerbüro e.V. jedes Jahr einen Kongress für ehrenamtliche Jugendliche, die sich in der Schule engagieren', 'UTF-8');
                }
                ?>
            </div>
            <!-- ./slogan -->
        </div>
        <!-- ./row -->


    </header>

    <main>
        <div class="content">
            <!-- The content of the webpage starts here -->
            <div class="row">
                <div class="col-md-12">
                    <?php if (have_posts()) : ?>
                        <?php $i = 1; ?>
                        <?php while (have_posts()) :
                            the_post(); ?>
                            <div class="article-wrapper">
                                <div class="article">
                                    <div class="pre_article">
                                        <img src="<?php bloginfo('template_directory'); ?>/images/boebel.png"
                                             width="30px"/>
                                        <a name="<?php echo $i; ?>"></a>

                                        <h2><?php the_title(); ?></h2>
                                    <span
                                        class="underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </div>
                                    <!-- ./pre_article -->
                                    <?php the_content(); ?>
                                    <div class="post_article">
                                        <a href="#<?php echo $i + 1; ?>"><img
                                                src="<?php bloginfo('template_directory'); ?>/images/pfeil.png"
                                                width="30px"/></a>
                                    </div>
                                    <!-- ./post_article -->
                                </div>
                                <!-- ./article -->
                            </div>
                            <!-- ./article-wrapper -->
                            <?php
                            $i++;
                            ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <!-- ./col-md-12 -->
            </div>
            <!-- ./row -->
        </div>
        <!-- ./content -->
    </main>

    <footer>
        <div class="row">
            <div class="col-md-12">
                <?php wp_nav_menu(array('theme_location' => 'footer-menu', 'container' => 'none')); ?>
                &copy; <?php echo date('Y'); ?> Münchner Schülerbüro e.V.
            </div>
        </div>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</div><!-- ./wraper -->
</body>
</html>