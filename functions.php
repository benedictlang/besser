<?php
/*
 * SCRIPTS
 */
function enqueue_my_scripts()
{
    wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', '1.9.1', false); // Bootstrap relies on we need the jquery library
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), true); // all the Bootstrap javascript goodness
    wp_enqueue_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js'); // sticky menu
    wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js'); // custom javascript functions
}

//add_action('wp_enqueue_scripts', 'enqueue_my_scripts');

/*
 * STYLESHEETS
 */

function enqueue_my_styles()
{
    wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('my-bootstrap-style', get_template_directory_uri() . '/style.css');
}

add_action('wp_enqueue_scripts', 'enqueue_my_styles');


/*
 * MENÜ
 */
function register_my_menu()
{
    register_nav_menu('main-menu', __('Hauptmenü'));
    register_nav_menu('footer-menu', __('Footermenü'));
}

add_action('init', 'register_my_menu');
?>

